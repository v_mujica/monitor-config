package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontConfigmail implements Serializable {
    private static final long serialVersionUID = 6181646831460265342L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idConfigMail;
    private String destinataries;
    private String cc;
    private String cco;
    private String inputParameters;
    @Lob
    private byte[] template;

}
