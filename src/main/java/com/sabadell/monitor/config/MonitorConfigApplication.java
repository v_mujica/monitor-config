package com.sabadell.monitor.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitorConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorConfigApplication.class, args);
    }

}
