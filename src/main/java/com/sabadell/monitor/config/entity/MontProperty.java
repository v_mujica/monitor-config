package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontProperty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String application;

    private String label;

    private String profile;

    @Column(length = 1024)
    private String propertyValue;

    @Column(length = 100)
    private String propertyKey;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "configmon_id", referencedColumnName = "id")
    private MontConfigmon montConfigmon;

}
