package com.sabadell.monitor.config.service.impl;

//import com.sabadell.monitor.common.entity.MontConfigmon;
//import com.sabadell.monitor.common.entity.MontQuery;

import com.sabadell.monitor.config.dto.ConfigmonDto;
import com.sabadell.monitor.config.dto.ItemsXAppsDto;
import com.sabadell.monitor.config.dto.QueryDto;
import com.sabadell.monitor.config.entity.MontConfigmon;
import com.sabadell.monitor.config.entity.MontProperty;
import com.sabadell.monitor.config.repository.ConfigmonRepository;
import com.sabadell.monitor.config.repository.ItemsXAppsRepository;
import com.sabadell.monitor.config.repository.PropertyRepository;
import com.sabadell.monitor.config.service.IConfigService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ConfigService implements IConfigService {

    private final static String INVALID_ID = "Id no válido";
    private final ItemsXAppsRepository itemsXAppsRepository;
    private final ConfigmonRepository configmonRepository;
    private final PropertyRepository propertyRepository;

    @Override
    public ArrayList<ItemsXAppsDto> getItemsXApps() {
        List<ItemsXAppsDto> collect = itemsXAppsRepository.findAll().stream()
                .map(monaItemsxapps -> ItemsXAppsDto.builder()
                        .id(monaItemsxapps.getId())
                        .finalDate(monaItemsxapps.getFinalDate())
                        .initialDate(monaItemsxapps.getInitialDate())
                        .appName(monaItemsxapps.getMoncApp().getName())
                        .itemName(monaItemsxapps.getMoncItems().getName())
                        .build())
                .collect(Collectors.toList());
        return new ArrayList<>(collect);
    }

    @Override
    public ArrayList<ConfigmonDto> getConfigmonByIdMontItemsXApps(Integer idMontItemsXApps) {
        MontConfigmon montConfigmon = configmonRepository.findById(getConfigId(idMontItemsXApps))
                .orElseThrow(() -> new IllegalArgumentException(INVALID_ID));
        ArrayList<ConfigmonDto> configmonDtos = new ArrayList<>();
        configmonDtos.add(getConfigmonDto(montConfigmon));
        return configmonDtos;
    }

    @Override
    public ConfigmonDto updateMontConfigmon(Integer idMontConfigmon, ConfigmonDto configmon) {
        MontConfigmon montConfigmon = configmonRepository.findById(getConfigId(idMontConfigmon))
                .orElseThrow(() -> new IllegalArgumentException(INVALID_ID));
        montConfigmon.setCron(configmon.getCron());
        montConfigmon.setPath(configmon.getPath());
        montConfigmon.setStatus(configmon.getStatus());
        montConfigmon.setDays(configmon.getDays());
        return getConfigmonDto(configmonRepository.save(montConfigmon));
    }

    @Override
    public ArrayList<QueryDto> getQueriesByIdMontConfigmon(Integer idMontConfigmon) {
        MontConfigmon montConfigmon = configmonRepository.findById(getConfigId(idMontConfigmon))
                .orElseThrow(() -> new IllegalArgumentException(INVALID_ID));
        List<MontProperty> montQueries = propertyRepository.findByMontConfigmon(montConfigmon);
        return new ArrayList<>(montQueries.stream().map(montProperty -> getMontQueryDto(montProperty)
        ).collect(Collectors.toList()));
    }

    @Override
    public QueryDto saveMontQuery(QueryDto queryDto) {
        return getMontQueryDto(propertyRepository.save(getMontQueryEntity(queryDto)));
    }


    @Override
    public QueryDto updateMontQuery(Integer idMontQuery, QueryDto queryDto) {
        MontProperty montProperty = getMontQueryEntity(queryDto);
        montProperty.setId(idMontQuery);
        return getMontQueryDto(propertyRepository.save(montProperty));
    }

    private MontConfigmon.MontConfigmonKey getConfigId(Integer id) {
        MontConfigmon.MontConfigmonKey montConfigmonKey = new MontConfigmon.MontConfigmonKey();
        montConfigmonKey.setId(id);
        return montConfigmonKey;
    }

    private ConfigmonDto getConfigmonDto(MontConfigmon montConfigmon) {
        return ConfigmonDto.builder()
                .id(montConfigmon.getMonaItemsxapps().getId()).cron(montConfigmon.getCron())
                .path(montConfigmon.getPath()).status(montConfigmon.getStatus())
                .days(montConfigmon.getDays()).build();
    }

    private QueryDto getMontQueryDto(MontProperty montProperty) {
        return QueryDto.builder()
                .id(montProperty.getId())
                .application(montProperty.getApplication())
                .label(montProperty.getLabel())
                .profile(montProperty.getProfile())
                //.description(montProperty.getDescription())
                .propertyValue(montProperty.getPropertyValue()).propertyKey(montProperty.getPropertyKey())
                .idConfigmon(montProperty.getMontConfigmon().getId().getId())
                .build();
    }

    private MontProperty getMontQueryEntity(QueryDto queryDto) {
        MontConfigmon montConfigmon = configmonRepository.findById(getConfigId(queryDto.getIdConfigmon()))
                .orElseThrow(() -> new IllegalArgumentException(INVALID_ID));
        MontProperty montProperty = new MontProperty();
        montProperty.setPropertyKey(queryDto.getPropertyKey());
        montProperty.setApplication(queryDto.getApplication());
        montProperty.setLabel(queryDto.getLabel());
        montProperty.setProfile(queryDto.getProfile());
        montProperty.setPropertyValue(queryDto.getPropertyValue());
        montProperty.setMontConfigmon(montConfigmon);
        return montProperty;
    }

}
