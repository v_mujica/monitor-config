package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
//@IdClass(MonaItemsxapps.class)
public class MontMontos implements Serializable {

    private static final long serialVersionUID = 7647041434305059984L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Double mount;

    private Timestamp date;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_item_app", referencedColumnName = "id", nullable = false)
    private MonaItemsxapps monaItemsxapps;
}
