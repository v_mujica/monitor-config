package com.sabadell.monitor.config.controller;

import com.sabadell.monitor.config.dto.ConfigmonDto;
import com.sabadell.monitor.config.dto.ItemsXAppsDto;
import com.sabadell.monitor.config.dto.QueryDto;
import com.sabadell.monitor.config.dto.ResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

public interface ConfigApi {
    @GetMapping("/itemsxapps")
    public ResponseEntity<ResponseDto<ArrayList<ItemsXAppsDto>>> getItemsXApps();

    @GetMapping("/montconfigmon")
    public ResponseEntity<ResponseDto<ArrayList<ConfigmonDto>>> getConfigmonByIdMontItemsXApps(
            @RequestParam("idMontItemsXApps") Integer idMontItemsXApps);

    @PutMapping("/montconfigmon")
    public ResponseEntity<ResponseDto<String>> updateMontConfigmon(
            @RequestParam("idMontConfigmon") Integer idMontConfigmon,
            @RequestBody ConfigmonDto configmon);

    @GetMapping("/montquery")
    public ResponseEntity<ResponseDto<ArrayList<QueryDto>>> getQueriesByIdMontConfigmon(
            @RequestParam("idMontConfigmon") Integer idMontConfigmon);

    @PostMapping("/montquery")
    public ResponseEntity<ResponseDto<String>> saveMontQuery(@RequestBody QueryDto queryDto);

    @PutMapping("/montquery")
    public ResponseEntity<ResponseDto<String>> updateMontQuery(
            @RequestParam("idMontQuery") Integer idMontQuery,
            @RequestBody QueryDto queryDto);
}
