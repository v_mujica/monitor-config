#!/bin/bash
app="young-meadow-68784"
mvn clean install -DskipTests &&
  heroku deploy:jar target/monitor-config-0.0.1-SNAPSHOT.jar --app $app
