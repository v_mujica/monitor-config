package com.sabadell.monitor.config.repository;

import com.sabadell.monitor.config.entity.MonaItemsxapps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemsXAppsRepository extends JpaRepository<MonaItemsxapps, Integer> {
}
