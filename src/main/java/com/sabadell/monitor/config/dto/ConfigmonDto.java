package com.sabadell.monitor.config.dto;

import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class ConfigmonDto implements Serializable {
    private static final long serialVersionUID = 3231301336882551071L;
    private Integer id;
    private String cron;
    private String path;
    private Integer status;
    private Integer days;
}
