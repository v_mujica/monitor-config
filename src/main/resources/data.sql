------------------------------------------------------------------------------
--monc_app
------------------------------------------------------------------------------
insert into monc_app ( id, final_date, initial_date, name, STATUS )
values ( 1, null, current_timestamp(), 'Banxico', 1 );

insert into monc_app ( id, final_date, initial_date, name, STATUS )
values ( 2, null, current_timestamp(), 'Bantotal', 1 );

insert into monc_app ( id, final_date, initial_date, name, STATUS )
values ( 3, null, current_timestamp(), 'Mobapp', 1 );

------------------------------------------------------------------------------
--monc_items
------------------------------------------------------------------------------
insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 1, null, current_timestamp(), 'SPEI', 1 );

insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 2, null, current_timestamp(), 'SPID', 1 );

insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 3, null, current_timestamp(), 'SWIF', 1 );

insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 4, null, current_timestamp(), 'CODI', 1 );

insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 5, null, current_timestamp(), 'UDIS', 1 );

insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 6, null, current_timestamp(), 'PayPal', 1 );

insert into monc_items ( id, final_date, initial_date, name, STATUS )
values ( 7, null, current_timestamp(), 'Sabadell Pago', 1 );
------------------------------------------------------------------------------
--mona_itemsxapps
------------------------------------------------------------------------------
insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 1 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 1 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 1 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 2 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 2 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 2 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 3 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 3 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 3 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 4 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 4 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 4 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 5 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 5 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 5 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 6 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 6 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 6 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 1, 7 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 2, 7 );

insert into mona_itemsxapps ( initial_date, idapp, iditem )
values ( current_timestamp(), 3, 7 );
------------------------------------------------------------------------------
--mont_configmon
------------------------------------------------------------------------------
insert into mont_configmon(id, cron, days, path, status)
values (1, 'cron1', 100, 'path1', 1);

insert into mont_configmon(id, cron, days, path, status)
values (2, 'cron2', 200, 'path2', 1);

insert into mont_configmon(id, cron, days, path, status)
values (3, 'cron1', 300, 'path3', 1);
------------------------------------------------------------------------------
--mont_query
------------------------------------------------------------------------------
--insert into mont_query(clave, description, value, configmon_id)
--values ('clave1', 'descripcion1', 'elQuery', 1);
--insert into mont_query(clave, description, value, configmon_id)
--values ('clave2', 'descripcion1', 'elQuery', 1);
--
--insert into mont_query(clave, description, value, configmon_id)
--values ('clave3', 'descripcion1', 'elQuery', 2);
--insert into mont_query(clave, description, value, configmon_id)
--values ('clave4', 'descripcion1', 'elQuery', 2);
--
--insert into mont_query(clave, description, value, configmon_id)
--values ('clave5', 'descripcion1', 'elQuery', 3);
--insert into mont_query(clave, description, value, configmon_id)
--values ('clave6', 'descripcion1', 'elQuery', 3);