package com.sabadell.monitor.config.repository;

import com.sabadell.monitor.config.entity.MontConfigmon;
import com.sabadell.monitor.config.entity.MontProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyRepository extends JpaRepository<MontProperty, Integer> {
    List<MontProperty> findByMontConfigmon(MontConfigmon montConfigmon);
}
