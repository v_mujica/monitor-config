package com.sabadell.monitor.config.dto;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueryDto implements Serializable {
    private static final long serialVersionUID = -2069798818535435685L;
    private Integer id;
    private String application;
    private String label;
    private String profile;
    private String propertyValue;
    private String propertyKey;
    private Integer idConfigmon;
}
