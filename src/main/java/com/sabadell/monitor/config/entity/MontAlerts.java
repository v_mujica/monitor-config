package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontAlerts implements Serializable {
    private static final long serialVersionUID = 9085129144170043726L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_status_alert", referencedColumnName = "idStatusAlert", nullable = false)
    private MoncStatusAlerts moncStatusAlerts;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_item_app", referencedColumnName = "id", nullable = false)
    private MonaItemsxapps monaItemsxapps;

    private Integer numOfNotifications;

    private Integer numOfOccurrences;

    private Timestamp initialDate;

    private Timestamp finalDate;
}

