package com.sabadell.monitor.config.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseDto<T extends Serializable> implements Serializable {
    private static final long serialVersionUID = -2332313785123733580L;
    private String message;
    private T entity;
}
