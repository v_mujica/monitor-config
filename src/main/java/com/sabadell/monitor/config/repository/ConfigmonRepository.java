package com.sabadell.monitor.config.repository;

import com.sabadell.monitor.config.entity.MontConfigmon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigmonRepository extends JpaRepository<MontConfigmon, MontConfigmon.MontConfigmonKey> {
}
