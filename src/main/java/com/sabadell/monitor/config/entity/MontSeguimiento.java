package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontSeguimiento implements Serializable {

    private static final long serialVersionUID = 7647041434305059984L;

    @EmbeddedId
    private MontSeguimientoKey id;

    // @Id
    @MapsId("id")
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "id")
    private MonaItemsxapps monaItemsxapps;
    private String description;
    private Timestamp executionDateActual;
    private Timestamp executionDatePrevious;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_id", referencedColumnName = "id")
    private MontSeguimientoStatus montSeguimientoStatus;

    @Data
    @Embeddable
    public static class MontSeguimientoKey implements Serializable {
        private static final long serialVersionUID = 1761720359425035189L;
        private Integer id;
    }
}

