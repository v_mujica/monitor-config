package com.sabadell.monitor.config.service;

import com.sabadell.monitor.config.dto.ConfigmonDto;
import com.sabadell.monitor.config.dto.ItemsXAppsDto;
import com.sabadell.monitor.config.dto.QueryDto;

import java.util.ArrayList;

public interface IConfigService {
    ArrayList<ItemsXAppsDto> getItemsXApps();

    ArrayList<ConfigmonDto> getConfigmonByIdMontItemsXApps(Integer idMontItemsXApps);

    ConfigmonDto updateMontConfigmon(Integer idMontConfigmon, ConfigmonDto configmon);

    ArrayList<QueryDto> getQueriesByIdMontConfigmon(Integer idMontConfigmon);

    QueryDto saveMontQuery(QueryDto queryDto);

    QueryDto updateMontQuery(Integer idMontQuery, QueryDto queryDto);
}
