package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MonhTransacciones implements Serializable {

    private static final long serialVersionUID = 4376896123311519340L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idTransaction;
    private Timestamp date;
    private Integer idStatusTransaction;
    private String address;
    private String account;
    private Double amount;
    private String trackingKey;
    private String beneficiaryAccount;
    private String orderingAccount;
    private Integer counterpartInstitution;
    private Integer operatingInstitution;
    private String beneficiaryName;

    //    @Column(name = "id_status")
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_status", referencedColumnName = "idStatus")
    private MoncEstadotransacciones moncEstadotransacciones;

}
