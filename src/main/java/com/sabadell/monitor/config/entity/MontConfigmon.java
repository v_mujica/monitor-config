package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontConfigmon implements Serializable {

    private static final long serialVersionUID = 7647041434305059984L;
    @EmbeddedId
    private MontConfigmonKey id;
    // @Id
    @MapsId("id")
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "id")
    private MonaItemsxapps monaItemsxapps;
    private Integer status;
    private String cron;
    private String path;
    private Integer days;

    @Data
    @Embeddable
    public static class MontConfigmonKey implements Serializable {
        private static final long serialVersionUID = -3393772469064960422L;
        private Integer id;
    }

}
