package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MonaItemsxapps implements Serializable {
    private static final long serialVersionUID = -8435377443320448625L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idapp", referencedColumnName = "id", nullable = false)
    private MoncApp moncApp;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "iditem", referencedColumnName = "id", nullable = false)
    private MoncItems moncItems;

    private Timestamp initialDate;

    private Timestamp finalDate;
}
