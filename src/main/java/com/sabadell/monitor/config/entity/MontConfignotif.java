package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontConfignotif implements Serializable {
    private static final long serialVersionUID = -7135950669953466241L;
    // @Id
    @EmbeddedId
    private MontConfignotifKey id;
    @MapsId("id")
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "id")
    private MonaItemsxapps monaItemsxapps;
    private Integer shouldNotify;
    private Integer numIntervalsToClose;
    private Integer numIntervalsToOpen;
    private Integer maxNumOfAlerts;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_config_mail", referencedColumnName = "idConfigMail")
    private MontConfigmail montConfigmail;

    @Data
    @Embeddable
    public static class MontConfignotifKey implements Serializable {
        private static final long serialVersionUID = 6090856366037662036L;
        private Integer id;
    }

}
