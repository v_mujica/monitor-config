package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MoncStatusAlerts implements Serializable {
    private static final long serialVersionUID = 8258663241006094104L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idStatusAlert;
    private String description;
    private Timestamp initialDate;
    private Timestamp finalDate;
}
