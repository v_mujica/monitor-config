package com.sabadell.monitor.config.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MontConteo implements Serializable {
    private static final long serialVersionUID = -1068432890345999004L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    private Double amount;
    private Timestamp date;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_item_app", referencedColumnName = "id", nullable = false)
    private MonaItemsxapps monaItemsxapps;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_status", referencedColumnName = "idStatus", nullable = false)
    private MoncEstadotransacciones moncEstadotransacciones;

}
