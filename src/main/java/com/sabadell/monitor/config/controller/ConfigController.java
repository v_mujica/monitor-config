package com.sabadell.monitor.config.controller;

import com.sabadell.monitor.config.dto.ConfigmonDto;
import com.sabadell.monitor.config.dto.ItemsXAppsDto;
import com.sabadell.monitor.config.dto.QueryDto;
import com.sabadell.monitor.config.dto.ResponseDto;
import com.sabadell.monitor.config.service.IConfigService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@AllArgsConstructor
public class ConfigController implements ConfigApi {

    private IConfigService configService;

    @Override
    public ResponseEntity<ResponseDto<ArrayList<ItemsXAppsDto>>> getItemsXApps() {
        ArrayList<ItemsXAppsDto> itemsXAppsDtos = configService.getItemsXApps();
        return ResponseEntity.ok(ResponseDto.<ArrayList<ItemsXAppsDto>>builder()
                .entity(itemsXAppsDtos).message("Items X Apps encontradas")
                .build());
    }

    @Override
    public ResponseEntity<ResponseDto<ArrayList<ConfigmonDto>>> getConfigmonByIdMontItemsXApps(Integer idMontItemsXApps) {
        ArrayList<ConfigmonDto> configmonDtos = configService.getConfigmonByIdMontItemsXApps(idMontItemsXApps);
        return ResponseEntity.ok(ResponseDto.<ArrayList<ConfigmonDto>>builder()
                .entity(configmonDtos).message("Configuraciones encontradas")
                .build());
    }

    @Override
    public ResponseEntity<ResponseDto<String>> updateMontConfigmon(Integer idMontConfigmon, ConfigmonDto configmon) {
        ConfigmonDto configmonDto = configService.updateMontConfigmon(idMontConfigmon, configmon);
        return ResponseEntity.ok(ResponseDto.<String>builder()
                .entity(configmonDto.toString()).message("Configuraciones actualizada")
                .build());
    }

    @Override
    public ResponseEntity<ResponseDto<ArrayList<QueryDto>>> getQueriesByIdMontConfigmon(Integer idMontConfigmon) {
        ArrayList<QueryDto> queryDtos = configService.getQueriesByIdMontConfigmon(idMontConfigmon);
        return ResponseEntity.ok(ResponseDto.<ArrayList<QueryDto>>builder()
                .entity(queryDtos).message("Queries encontrados")
                .build());
    }

    @Override
    public ResponseEntity<ResponseDto<String>> saveMontQuery(QueryDto queryDto) {
        QueryDto queryDto1 = configService.saveMontQuery(queryDto);
        return ResponseEntity.ok(ResponseDto.<String>builder()
                .entity(queryDto1.toString()).message("Configuracion guardado.")
                .build());
    }

    @Override
    public ResponseEntity<ResponseDto<String>> updateMontQuery(Integer idMontQuery, QueryDto queryDto) {
        QueryDto queryDto1 = configService.updateMontQuery(idMontQuery, queryDto);
        return ResponseEntity.ok(ResponseDto.<String>builder()
                .entity(queryDto1.toString()).message("Configuracion actualizado")
                .build());
    }


}
